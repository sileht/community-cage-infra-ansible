$TTL 1D
@				IN SOA		ns1.osci.io. hostmaster.osci.io. (
						2016120100	; serial (YYYYMMDDRR)
						3600		; Refresh
						1800		; Retry
						604800		; Timeout
						86400 )		; Negative answer TTL
				IN NS		ns1
				IN NS		ns2
				IN MX		10	mx1
				IN MX		20	mx2
; TODO: main website (domain A)

; physical machines
speedy				IN A		8.43.85.225
guido				IN A		8.43.85.226

; basic domain services
polly				IN A		8.43.85.229
francine			IN A		8.43.85.230
ns1				IN A		8.43.85.229
ns2				IN A		8.43.85.230
; keep me A/AAAA RR please, CNAME would break mails
mx1				IN A		8.43.85.229
mx2				IN A 		8.43.85.230
ntp				IN CNAME	8.43.85.229

; web builders
osas-community-web-builder	IN A		8.43.85.227
ovirt-web-builder		IN A		8.43.85.228
; TODO: RDO web builder

; websites
; TODO: main website (www CNAME)
tickets				IN A		8.43.85.231

